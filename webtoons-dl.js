const
    parseRanges = str => str
        .split(',')
        .flatMap(chunk => {
            const
                [
                    strA,
                    strB
                ] = chunk.split('-'),
                a = parseInt(strA),
                b = strB && parseInt(strB);
            return Array(strB ? b + 1 - a : 1).fill(a).map((x, y) => x + y);
        }),
    [
        comicId,
        episodeRanges
    ] = Deno.args,
    episodeNumbers = parseRanges(episodeRanges),
    comicRes = await (await fetch(`https://www.webtoons.com/en/*/*/list?title_no=${comicId}`)).text(),
    comicTitle = comicRes.match(/<title>(.+) \|/)[1],
    comicEpisodeCount = episodeNumbers.length,
    rootDirectory = `./data/${comicTitle}`;

try { await Deno.mkdir(rootDirectory); } catch(_){}

for(let episodeIndex = 0; episodeIndex < comicEpisodeCount; episodeIndex++){
    const comicEpisodeNumber = episodeNumbers[episodeIndex];

    console.log(`Downloading episode ${comicEpisodeNumber} (${episodeIndex}/${comicEpisodeCount})`);

    const episodeReq = await fetch(`https://www.webtoons.com/en/*/*/*/viewer?title_no=${comicId}&episode_no=${comicEpisodeNumber}`);
    const episodeRes = await episodeReq.text();

    const episodeStrips = [...episodeRes.matchAll(/_images" data-url="([^"]+)"/g)].map(match => match[1]);
    const episodeStripsCount = episodeStrips.length;

    for(let stripIndex = 0; stripIndex < episodeStripsCount; stripIndex++){
        console.log(`Downloading episode ${comicEpisodeNumber} (${episodeIndex}/${comicEpisodeCount}) strip ${stripIndex + 1}/${episodeStripsCount}`);

        const stripReq = await fetch(episodeStrips[stripIndex], {
            headers: {
                referer: episodeReq.url
            }
        });
        const stripBuffer = await stripReq.arrayBuffer();

        await Deno.writeFile(`${rootDirectory}/${comicEpisodeNumber}_${stripIndex + 1}.jpg`, new Uint8Array(stripBuffer));
    }
}